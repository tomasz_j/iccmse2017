package pl.polsl.tjastrzab.iccmse.algorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import pl.polsl.tjastrzab.iccmse.model.Edge;
import pl.polsl.tjastrzab.iccmse.model.Graph;
import pl.polsl.tjastrzab.iccmse.model.Interval;
import pl.polsl.tjastrzab.iccmse.model.Path;

/**
 * Class resolving the meetings
 *
 * @author Tomasz Jastrz�b
 * @version 1.0
 */
public class Algorithm {

	/** Graph */
	private final Graph graph;
	/** List of models used before the meetings resolution */
	private final List<Model> models = new ArrayList<>();

	/**
	 * Constructor
	 *
	 * @param graph
	 *            the graph
	 */
	public Algorithm(Graph graph) {
		this.graph = graph;
	}

	/**
	 * Adds model
	 *
	 * @param model
	 *            model to be added
	 */
	public void addModel(Model model) {
		models.add(model);
	}

	/**
	 * Computes total evacuation time
	 *
	 * @param counts
	 *            number of miners for given speed
	 * @return total evacuation time
	 */
	public double computeTotalTime(int[] counts) {
		double totalTime = 0.0;

		for (int i = 0; i < models.size(); i++) {
			for (Map.Entry<Edge, List<Interval>> entry : models.get(i)
					.getOccupationIntervals().entrySet()) {
				if (entry.getKey().getVertex2().equals(graph.getSource())) {
					for (Interval interval : entry.getValue()) {
						totalTime += counts[i] * interval.getTimeE();
					}
				}
			}
		}
		return totalTime;
	}

	/**
	 * Finds conflicts - meeting points in the corridors
	 */
	public void findConflicts() {
		boolean changed;
		Model model1, model2;
		List<Interval> intervals;
		double sTime, eTime, delay;
		Set<Map.Entry<Edge, List<Interval>>> entries;

		do {
			changed = false;
			main_loop: for (int i = 0; i < models.size(); i++) {
				model1 = models.get(i);
				entries = model1.getOccupationIntervals().entrySet();
				for (Map.Entry<Edge, List<Interval>> entry : entries) {
					for (int j = i; j < models.size(); j++) {
						model2 = models.get(j);
						intervals = model2.getOccupationIntervals().get(
								entry.getKey());
						if (intervals != null) {
							for (Interval interval1 : entry.getValue()) {
								sTime = interval1.getTimeS();
								eTime = interval1.getTimeE();
								for (Interval interval2 : intervals) {
									if (sTime != interval2.getTimeS()) {
										if (model1.getSpeed() >= model2
												.getSpeed()) {
											if (sTime > interval2.getTimeS()
													&& eTime > interval2
															.getTimeS()
													&& eTime < interval2
															.getTimeE()) {
												changed = true;
												delay = interval2.getTimeE()
														- eTime;
												recomputeIntervals(model1,
														interval1,
														entry.getKey(), delay);
												break main_loop;

											}
										} else if (model1.getSpeed() < model2
												.getSpeed()) {
											if (sTime < interval2.getTimeS()
													&& interval2.getTimeE() > sTime
													&& interval2.getTimeE() < eTime) {
												changed = true;
												delay = eTime
														- interval2.getTimeE();
												recomputeIntervals(model1,
														interval2,
														entry.getKey(), delay);
												break main_loop;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} while (changed);
	}

	/**
	 * Recomputes occupation intervals after delay is detected
	 *
	 * @param algorithm
	 *            model in which the intervals are to be recomputed
	 * @param interval
	 *            interval that changed
	 * @param edge
	 *            edge connected with the interval
	 * @param delay
	 *            detected delay
	 */
	private void recomputeIntervals(Model algorithm, Interval interval,
			Edge edge, double delay) {
		int idx = -1;
		Interval intTmp;
		Path path = interval.getPath();
		Map<Edge, List<Interval>> intervals = algorithm
				.getOccupationIntervals();

		intTmp = interval;
		idx = path.getEdgePosition(edge);
		for (int i = idx + 1; i < path.getEdgesCount(); i++) {
			for (Interval intrv : intervals.get(path.getEdge(i))) {
				if (intrv.getTimeS() == intTmp.getTimeE()) {
					intTmp = intrv;
					intrv.setTimeS(intrv.getTimeS() + delay);
					intrv.setTimeE(intrv.getTimeE() + delay);
					break;
				}
			}
		}
		interval.setTimeE(interval.getTimeE() + delay);
	}
}
