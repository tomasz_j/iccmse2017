package pl.polsl.tjastrzab.iccmse.algorithm;

import pl.polsl.tjastrzab.iccmse.model.Graph;

/**
 * Class representing solving thread
 *
 * @author Tomasz Jastrz�b
 * @version 1.0
 */
public class ModelThread extends Thread {

	/** Model for Dijkstra algorithm application */
	private final Model model;

	/**
	 * Constructor
	 *
	 * @param graph
	 *            graph
	 * @param speedNo
	 *            speed index
	 * @param speed
	 *            speed value
	 */
	public ModelThread(Graph graph, int speedNo, double speed) {
		model = new Model(graph, speedNo, speed);
	}

	/**
	 * Getter method
	 *
	 * @return the model
	 */
	public Model getModel() {
		return model;
	}

	public void run() {
		model.runDijkstra();
		model.computePaths();
		model.computeOccupationIntervals();
	}

}
