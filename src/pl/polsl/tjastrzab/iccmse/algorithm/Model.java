package pl.polsl.tjastrzab.iccmse.algorithm;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import pl.polsl.tjastrzab.iccmse.model.Edge;
import pl.polsl.tjastrzab.iccmse.model.Graph;
import pl.polsl.tjastrzab.iccmse.model.Interval;
import pl.polsl.tjastrzab.iccmse.model.Path;
import pl.polsl.tjastrzab.iccmse.model.Vertex;

/**
 * Class representing the model before meetings resolution
 *
 * @author Tomasz Jastrz�b
 * @version 1.0
 */
public class Model {

	/** List of paths for each vertex */
	private List<Path> paths;
	/** Spped index within array of speeds */
	private final int speedNo;
	/** Graph */
	private final Graph graph;
	/** Speed value */
	private final double speed;
	/** Array of successors, used in Dijkstra algorithm */
	private Vertex[] successors;
	/** Map of time intervals of edge ''occupation'' by a miiner */
	private Map<Edge, List<Interval>> occupationIntervals;

	/**
	 * Constructor
	 *
	 * @param graph
	 *            graph
	 * @param speedNo
	 *            speed index
	 * @param speed
	 *            speed value
	 */
	public Model(Graph graph, int speedNo, double speed) {
		this.graph = graph;
		this.speed = speed;
		this.speedNo = speedNo;
	}

	/**
	 * Finds occupation intervals for graph edges
	 */
	public void computeOccupationIntervals() {
		Path path;
		int eCount = graph.getEdgesCount();
		int vCount = graph.getVertexCount();

		occupationIntervals = new HashMap<Edge, List<Interval>>(eCount);
		for (int i = 0; i < eCount; i++) {
			occupationIntervals
					.put(graph.getEdge(i), new ArrayList<Interval>());
		}
		for (int i = 0; i < vCount; i++) {
			path = paths.get(i);
			for (int j = 0; j < path.getEdgesCount(); j++) {
				occupationIntervals.get(path.getEdge(j)).add(
						new Interval(path, path.getTime(path.getEdge(j)
								.getVertex1()), path.getTime(path.getEdge(j)
								.getVertex2())));
			}
		}
		for (int i = 0; i < eCount; i++) {
			if (occupationIntervals.get(graph.getEdge(i)).isEmpty()) {
				occupationIntervals.remove(graph.getEdge(i));
			}
		}
	}

	/**
	 * Finds the paths between each node and safe exit point
	 */
	public void computePaths() {
		Path path;
		double sum;
		Vertex current, next;
		int count = graph.getVertexCount();

		paths = new ArrayList<>(count);
		for (int i = 0; i < count; i++) {
			sum = 0.0;
			path = new Path();
			path.addPoint(current = graph.getVertex(i), 0.0);
			while ((next = successors[current.getNumber()]) != null) {
				for (Edge edge : graph.getInEdges(next)) {
					if (edge.getVertex1().equals(current)) {
						sum += edge.getWeight() / speed;
						path.addEdge(edge);
						break;
					}
				}
				path.addPoint(next, sum);
				current = next;
			}
			paths.add(path);
		}
	}

	/**
	 * Getter method
	 *
	 * @return map of occupation intervals
	 */
	public Map<Edge, List<Interval>> getOccupationIntervals() {
		return occupationIntervals;
	}

	/**
	 * Getter method
	 *
	 * @return list of paths
	 */
	public List<Path> getPaths() {
		return paths;
	}

	/**
	 * Getter method
	 *
	 * @return speed
	 */
	public double getSpeed() {
		return speed;
	}

	/**
	 * Executes Dijkstra algorithm for the graph
	 */
	public void runDijkstra() {
		Vertex current;
		int count = graph.getVertexCount();
		PriorityQueue<Vertex> queue = new PriorityQueue<Vertex>(
				new Comparator<Vertex>() {
					@Override
					public int compare(Vertex o1, Vertex o2) {
						return Double.valueOf(o1.getCurrentWeight(speedNo))
								.compareTo(o2.getCurrentWeight(speedNo));
					}
				});

		successors = new Vertex[count];
		for (int i = 0; i < count; i++) {
			graph.getVertex(i).setCurrentWeight(speedNo, Double.MAX_VALUE);
		}
		graph.getSource().setCurrentWeight(speedNo, 0);
		for (int i = 0; i < count; i++) {
			queue.add(graph.getVertex(i));
		}
		while (!queue.isEmpty()) {
			current = queue.poll();
			for (Edge edge : graph.getInEdges(current)) {
				if (edge.getVertex1().getCurrentWeight(speedNo) > current
						.getCurrentWeight(speedNo) + edge.getWeight() / speed) {
					edge.getVertex1().setCurrentWeight(
							speedNo,
							current.getCurrentWeight(speedNo)
									+ edge.getWeight() / speed);
					successors[edge.getVertex1().getNumber()] = current;
				}
			}
		}
	}
}
