package pl.polsl.tjastrzab.iccmse.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing the path between given vertex and safe exit point 
 *
 * @author Tomasz Jastrz�b
 * @version 1.0
 */
public class Path {

	/** List of edges */
	private List<Edge> edges;
	/** List of times of reaching given vertex */
	private List<Double> times;
	/** List of vertices */
	private List<Vertex> vertices;

	/**
	 * Constructor
	 */
	public Path() {
		this.edges = new ArrayList<>();
		this.times = new ArrayList<>();
		this.vertices = new ArrayList<>();
	}

	/**
	 * Adds edge to the list
	 *
	 * @param edge	the edge to be added
	 */
	public void addEdge(Edge edge) {
		edges.add(edge);
	}

	/**
	 * Adds vertex to the path
	 *
	 * @param vertex	the vertex to be added
	 * @param time		the time of reaching given vertex
	 */
	public void addPoint(Vertex vertex, double time) {
		times.add(time);
		vertices.add(vertex);
	}

	/**
	 * Gets edge under given index
	 *
	 * @param idx	index
	 * @return		edge under given index
	 */
	public Edge getEdge(int idx) {
		return edges.get(idx);
	}

	/**
	 * Gets the position of an edge within the list of edges
	 *
	 * @param edge		edge to find
	 * @return			posiition of an edge within the list of edges
	 */
	public int getEdgePosition(Edge edge) {
		return edges.indexOf(edge);
	}

	/**
	 * Gets number of edges
	 *
	 * @return	number of edges
	 */
	public int getEdgesCount() {
		return edges.size();
	}

	/**
	 * Gets number of vertices
	 *
	 * @return	number of vertices
	 */
	public int getPointsCount() {
		return vertices.size();
	}

	/**
	 * Gets time of reaching given vertex
	 *
	 * @param vertex		vertex to reach
	 * @return				time of reaching given vertex
	 */
	public double getTime(Vertex vertex) {
		return times.get(vertices.indexOf(vertex));
	}

	/**
	 * Gets vertex under given index
	 *
	 * @param idx		index
	 * @return			vertex under given index
	 */
	public Vertex getVertex(int idx) {
		return vertices.get(idx);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < vertices.size(); i++) {
			sb.append(vertices.get(i).getNumber());
			sb.append(": ");
			sb.append(times.get(i));
			if (i < vertices.size() - 1) {
				sb.append(", ");
			}
		}
		sb.append(" ; ");
		for (int i = 0; i < edges.size(); i++) {
			sb.append(edges.get(i).getNumbers());
			if (i < edges.size() - 1) {
				sb.append(", ");
			}
		}
		return sb.toString();
	}
}
