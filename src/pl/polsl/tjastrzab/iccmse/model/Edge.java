package pl.polsl.tjastrzab.iccmse.model;

/**
 * Class representing edge of a graph
 *
 * @author Tomasz Jastrz�b
 * @version 1.0
 */
public class Edge {

	/** Starting vertex */
	private Vertex vertex1;
	/** Ending vertex */
	private Vertex vertex2;
	/** Edge weight */
	private double weight;
	/** Flag indicating whether given edge was excluded - not used */
	private boolean excluded;

	/**
	 * Gets the information on the vertices connected with the edge
	 *
	 * @return vertex numbers
	 */
	public String getNumbers() {
		return vertex1.getNumber() + "-" + vertex2.getNumber();
	}

	/**
	 * Getter method
	 *
	 * @return starting vertex
	 */
	public Vertex getVertex1() {
		return vertex1;
	}

	/**
	 * Getter method
	 *
	 * @return ending vertex
	 */
	public Vertex getVertex2() {
		return vertex2;
	}

	/**
	 * Getter method
	 *
	 * @return the weight
	 */
	public double getWeight() {
		return weight;
	}

	/**
	 * Getter method
	 *
	 * @return Flag indicating whether given edge was excluded
	 */
	public boolean isExcluded() {
		return excluded;
	}

	/**
	 * Setter method
	 *
	 * @param excluded
	 *            Flag indicating whether given edge was excluded
	 */
	public void setExcluded(boolean excluded) {
		this.excluded = excluded;
	}

	/**
	 * Setter method
	 *
	 * @param vertex1
	 *            starting vertex
	 */
	public void setVertex1(Vertex vertex1) {
		this.vertex1 = vertex1;
	}

	/**
	 * Setter method
	 *
	 * @param vertex2
	 *            ending vertex
	 */
	public void setVertex2(Vertex vertex2) {
		this.vertex2 = vertex2;
	}

	/**
	 * Setter method
	 *
	 * @param weight
	 *            the weight to set
	 */
	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String toString() {
		return vertex1.getNumber() + "-" + vertex2.getNumber() + ": " + weight;
	}
}
