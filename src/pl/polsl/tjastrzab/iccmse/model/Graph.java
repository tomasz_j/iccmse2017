package pl.polsl.tjastrzab.iccmse.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

/**
 * Class representing a graph
 *
 * @author Tomasz Jastrz�b
 * @version 1.0
 */
public class Graph {

	/**
	 * Generates graph
	 *
	 * @param n
	 *            vertex count
	 * @param wMin
	 *            min weight
	 * @param wMax
	 *            max weight
	 * @param p1
	 *            probability of connection existence
	 * @param p2
	 *            probability of steep corridor
	 * @param speedsCnt
	 *            number of different speeds
	 * @return created graph
	 */
	public static Graph createGraph(int n, int wMin, int wMax, double p1,
			double p2, int speedsCnt) {
		Graph graph = new Graph();
		Random random = new Random();
		double weight, sqrt = Math.sqrt(2);

		for (int i = 0; i < n; i++) {
			graph.addVertex(new Vertex(i, speedsCnt, i == (n - 1)));
		}
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				if (Math.random() <= p1) {
					weight = random.nextInt(wMax - wMin + 1) + wMin;
					graph.addEdge(graph.getVertex(i), graph.getVertex(j),
							weight, Math.random() <= p2 ? weight / sqrt
									: weight);
				}
			}
		}
		return graph;
	}

	/**
	 * Reads graph from file
	 *
	 * @param file
	 *            input file
	 * @param speedsCnt
	 *            speeds count
	 * @return graph
	 * @throws FileNotFoundException
	 *             thrown in case the file does not exist
	 * @throws IOException
	 *             thrown in case of some I/O error
	 */
	public static Graph readGraph(String file, int speedsCnt)
			throws FileNotFoundException, IOException {
		String line;
		double w1 = 0, w2 = 0;
		Graph graph = new Graph();
		int i = 0, n, v1 = 0, v2 = 0;

		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			while ((line = br.readLine()) != null) {
				if (i == 0) {
					n = line.split(", ").length;
					for (int j = 0; j < n; j++) {
						graph.addVertex(new Vertex(j, speedsCnt, j == (n - 1)));
					}
				}
				if (i % 2 == 1) {
					w1 = Double.parseDouble(line.split(": ")[1]);
					v1 = Integer.parseInt(line.split(": ")[0].split("-")[0]);
					v2 = Integer.parseInt(line.split(": ")[0].split("-")[1]);
				} else if (i > 0 && i % 2 == 0) {
					w2 = Double.parseDouble(line.split(": ")[1]);
					graph.addEdge(graph.getVertex(v1), graph.getVertex(v2), w1,
							w2);
				}
				i++;
			}
		}
		return graph;
	}

	/** Source vertex */
	private Vertex source;
	/** List of edges */
	private final List<Edge> edges = new ArrayList<>();
	/** List of vertices */
	private final List<Vertex> vertices = new ArrayList<>();
	/** Map of incoming edges for each vertex */
	private final Map<Vertex, List<Edge>> inEdges = new HashMap<>();

	/**
	 * Adds edges to the graph
	 *
	 * @param vertex1
	 *            1st vertex
	 * @param vertex2
	 *            2nd vertex
	 * @param weight12
	 *            weight in the direction 1->2
	 * @param weight21
	 *            weight in the direction 2->1
	 */
	public void addEdge(Vertex vertex1, Vertex vertex2, double weight12,
			double weight21) {
		Edge edge1 = new Edge();
		Edge edge2 = new Edge();

		edge1.setVertex1(vertex1);
		edge1.setVertex2(vertex2);
		edge1.setWeight(weight12);
		edge2.setVertex1(vertex2);
		edge2.setVertex2(vertex1);
		edge2.setWeight(weight21);
		edges.add(edge1);
		edges.add(edge2);
		if (!inEdges.containsKey(vertex1)) {
			inEdges.put(vertex1, new ArrayList<>());
		}
		inEdges.get(vertex1).add(edge2);
		if (!inEdges.containsKey(vertex2)) {
			inEdges.put(vertex2, new ArrayList<>());
		}
		inEdges.get(vertex2).add(edge1);
	}

	/**
	 * Adds vertex to the graph
	 *
	 * @param v
	 *            vertex to be added
	 */
	public void addVertex(Vertex v) {
		vertices.add(v);
		if (v.isSafe()) {
			source = v;
		}
	}

	/**
	 * Gets edge under given index
	 *
	 * @param idx
	 *            index
	 * @return edge under given index
	 */
	public Edge getEdge(int idx) {
		return edges.get(idx);
	}

	/**
	 * Gets number of edges
	 *
	 * @return number of edges
	 */
	public int getEdgesCount() {
		return edges.size();
	}

	/**
	 * Gets incoming edges for vertex
	 *
	 * @param vertex
	 *            vertex
	 * @return incoming edges for vertex
	 */
	public List<Edge> getInEdges(Vertex vertex) {
		return inEdges.get(vertex);
	}

	/**
	 * Gets source (safe exit) vertex
	 *
	 * @return safe exit vertex
	 */
	public Vertex getSource() {
		return source;
	}

	/**
	 * Gets vertex under given index
	 *
	 * @param idx
	 *            index
	 * @return vertex under given index
	 */
	public Vertex getVertex(int idx) {
		return vertices.get(idx);
	}

	/**
	 * Gets number of vertices
	 *
	 * @return number of vertices
	 */
	public int getVertexCount() {
		return vertices.size();
	}

	/**
	 * Saves graph to file
	 *
	 * @param file
	 *            output file
	 * @throws IOException
	 *             thrown in case of I/O error
	 */
	public void saveGraph(String file) throws IOException {
		DecimalFormat fmt = new DecimalFormat("###0.00",
				new DecimalFormatSymbols(Locale.ENGLISH));

		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			for (int i = 0; i < vertices.size(); i++) {
				bw.write(Integer.toString(vertices.get(i).getNumber()));
				if (i < vertices.size() - 1) {
					bw.write(", ");
				}
			}
			bw.newLine();
			for (int i = 0; i < edges.size(); i++) {
				bw.write(edges.get(i).getNumbers());
				bw.write(": ");
				bw.write(fmt.format(edges.get(i).getWeight()));
				bw.newLine();
			}
		}
	}

	/**
	 * Converts graph to a user-friendly string
	 *
	 * @return string describing the graph
	 */
	public String toUserFriendlyString() {
		StringBuilder sb = new StringBuilder();
		DecimalFormat fmt = new DecimalFormat("###0.00",
				new DecimalFormatSymbols(Locale.ENGLISH));

		sb.append("Vertices: ");
		for (int i = 0; i < vertices.size(); i++) {
			sb.append(vertices.get(i).getNumber());
			if (i < vertices.size() - 1) {
				sb.append(", ");
			}
		}
		sb.append(System.lineSeparator()).append(System.lineSeparator());
		sb.append("Flat corridors: ").append(System.lineSeparator());
		for (int i = 0; i < edges.size(); i += 2) {
			if (edges.get(i).getWeight() == edges.get(i + 1).getWeight()) {
				sb.append("(");
				sb.append(edges.get(i).getVertex1().getNumber());
				sb.append(", ");
				sb.append(edges.get(i).getVertex2().getNumber());
				sb.append(", ");
				sb.append(fmt.format(edges.get(i).getWeight()));
				sb.append(")");
				sb.append(System.lineSeparator());
			}
		}
		sb.append(System.lineSeparator());
		sb.append("Steep corridors: ").append(System.lineSeparator());
		for (int i = 0; i < edges.size(); i += 2) {
			if (edges.get(i).getWeight() != edges.get(i + 1).getWeight()) {
				sb.append("(");
				sb.append(edges.get(i).getVertex1().getNumber());
				sb.append(", ");
				sb.append(edges.get(i).getVertex2().getNumber());
				sb.append(", ");
				sb.append(fmt.format(edges.get(i).getWeight()));
				sb.append(", ");
				sb.append(fmt.format(edges.get(i + 1).getWeight()));
				sb.append(")");
				sb.append(System.lineSeparator());
			}
		}
		return sb.toString();
	}

}
