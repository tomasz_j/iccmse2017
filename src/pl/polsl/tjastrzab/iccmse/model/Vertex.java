package pl.polsl.tjastrzab.iccmse.model;

/**
 * Class representing vertex of a graph
 *
 * @author Tomasz Jastrz�b
 * @version 1.0
 */
public class Vertex {

	/** Flag indicating whether given vertex is an exit point */
	private boolean safe;
	/** Vertex number */
	private final int number;
	/** Flag indicating whether given vertex was excluded - not used */
	private boolean excluded;
	/**
	 * Array of weights - times of reaching the vertex. Cells are dedicated to
	 * different speeds
	 */
	private double[] currentWeights;

	/**
	 * Constructor
	 *
	 * @param number
	 *            vertex number
	 * @param speedsCount
	 *            number of different speeds
	 * @param safe
	 *            flag indicating safe exit point
	 */
	public Vertex(int number, int speedsCount, boolean safe) {
		this.safe = safe;
		this.number = number;
		this.currentWeights = new double[speedsCount];
	}

	/**
	 * Gets current weight for given speed index
	 *
	 * @param idx
	 *            speed index
	 * @return current weight
	 */
	public double getCurrentWeight(int idx) {
		return currentWeights[idx];
	}

	/**
	 * Getter method
	 *
	 * @return the vertex number
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * Getter method
	 *
	 * @return flag indicating node exclusion
	 */
	public boolean isExcluded() {
		return excluded;
	}

	/**
	 * Getter method
	 *
	 * @return flag indicating safe exit point
	 */
	public boolean isSafe() {
		return safe;
	}

	/**
	 * Sets current weight
	 *
	 * @param idx				speed index
	 * @param currentWeight		current weight
	 */
	public void setCurrentWeight(int idx, double currentWeight) {
		this.currentWeights[idx] = currentWeight;
	}

	/**
	 * Setter method
	 *
	 * @param excluded
	 *            flag indicating node exclusion
	 */
	public void setExcluded(boolean excluded) {
		this.excluded = excluded;
	}

	/**
	 * Setter method
	 *
	 * @param safe
	 *            flag indicating safe exit point
	 */
	public void setSafe(boolean safe) {
		this.safe = safe;
	}

	public String toString() {
		return number + " " + safe + " " + excluded;
	}
}
