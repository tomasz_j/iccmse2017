package pl.polsl.tjastrzab.iccmse.model;

/**
 * Class representing time interval
 *
 * @author Tomasz Jastrz�b
 * @version 1.0
 */
public class Interval {

	/** Path within which the time interval occurs */
	private Path path;
	/** Start time */
	private double timeS;
	/** End time */
	private double timeE;

	/**
	 * Constructor
	 *
	 * @param path
	 *            path
	 * @param timeS
	 *            start time
	 * @param timeE
	 *            end time
	 */
	public Interval(Path path, double timeS, double timeE) {
		this.path = path;
		this.timeS = timeS;
		this.timeE = timeE;
	}

	/**
	 * Getter method
	 *
	 * @return the path
	 */
	public Path getPath() {
		return path;
	}

	/**
	 * Setter method
	 *
	 * @param path
	 *            the path to set
	 */
	public void setPath(Path path) {
		this.path = path;
	}

	/**
	 * Getter method
	 *
	 * @return the start time
	 */
	public double getTimeS() {
		return timeS;
	}

	/**
	 * Setter method
	 *
	 * @param timeS
	 *            the start time
	 */
	public void setTimeS(double timeS) {
		this.timeS = timeS;
	}

	/**
	 * Getter method
	 *
	 * @return the end time
	 */
	public double getTimeE() {
		return timeE;
	}

	/**
	 * Setter method
	 *
	 * @param timeE
	 *            the end time
	 */
	public void setTimeE(double timeE) {
		this.timeE = timeE;
	}

	public String toString() {
		return "[" + timeS + ", " + timeE + "]";
	}
}
