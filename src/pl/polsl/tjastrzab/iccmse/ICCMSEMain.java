package pl.polsl.tjastrzab.iccmse;

import java.io.IOException;

import pl.polsl.tjastrzab.iccmse.algorithm.Algorithm;
import pl.polsl.tjastrzab.iccmse.algorithm.ModelThread;
import pl.polsl.tjastrzab.iccmse.model.Graph;
import pl.polsl.tjastrzab.iccmse.model.Vertex;

/**
 * Main class
 *
 * @author Tomasz Jastrz�b
 * @version 1.0
 */
public class ICCMSEMain {

	/** Vertex count */
	private static final int N = 25;
	/** Minimum weight */
	private static final int W_MIN = 1;
	/** Maximum weight */
	private static final int W_MAX = 300;
	/** Probability that an edge exists between two vertices */
	private static final double P1 = 0.3;
	/** Probability that an edge is steep */
	private static final double P2 = 0.3;
	/** Array of speeds */
	private static final double[] SPEEDS = { 65, 55, 45, 35, 30, 24, 15, 11, 6,
			5 };

	/**
	 * Creates graph
	 *
	 * @param file
	 *            output file
	 */
	public static void createGraph(String file) {
		Graph graph = Graph.createGraph(N, W_MIN, W_MAX, P1, P2, SPEEDS.length);

		try {
			graph.saveGraph(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creates exemplary graph
	 *
	 * @return graph
	 */
	public static Graph createGraphExample() {
		Graph graph = new Graph();

		for (int i = 0; i < 6; i++) {
			graph.addVertex(new Vertex(i, SPEEDS.length, i == 4));
		}
		graph.addEdge(graph.getVertex(0), graph.getVertex(1), 7, 5);
		graph.addEdge(graph.getVertex(0), graph.getVertex(2), 9, 3);
		graph.addEdge(graph.getVertex(0), graph.getVertex(5), 14, 10);
		graph.addEdge(graph.getVertex(1), graph.getVertex(2), 10, 2);
		graph.addEdge(graph.getVertex(1), graph.getVertex(3), 15, 8);
		graph.addEdge(graph.getVertex(2), graph.getVertex(3), 11, 1);
		graph.addEdge(graph.getVertex(2), graph.getVertex(5), 2, 2);
		graph.addEdge(graph.getVertex(5), graph.getVertex(4), 9, 5);
		graph.addEdge(graph.getVertex(3), graph.getVertex(4), 6, 6);
		return graph;
	}

	/**
	 * Main method
	 *
	 * @param args
	 *            -cg <output_file> to generate and save the graph -sg
	 *            <input_file> <list_of_speed_indices> to solve a problem for
	 *            given speeds speed indices have to be between 0 and 9
	 *            inclusive
	 */
	public static void main(String[] args) {
		int[] counts = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };

		switch (args[0]) {
		case "-cg":
			createGraph(args[1]);
			break;
		case "-sg":
			solveGraph(args, counts);
			break;
		}

	}

	/**
	 * Executes the parallel algorithm
	 *
	 * @param args
	 *            <input_file> <list_of_speed_indices>
	 * @param counts
	 *            counts of miners for each speed
	 */
	public static void solveGraph(String[] args, int[] counts) {
		Graph graph;
		long sTime, eTime;
		Algorithm algorithm;
		ModelThread[] threads;
		boolean allDone = false;
		double totalTime1, totalTime2;

		try {
			threads = new ModelThread[args.length - 2];
			graph = Graph.readGraph(args[1], args.length - 2);
			sTime = System.currentTimeMillis();
			for (int i = 2; i < args.length; i++) {
				threads[i - 2] = new ModelThread(graph, i - 2,
						SPEEDS[Integer.parseInt(args[i])]);
				threads[i - 2].start();
			}
			do {
				allDone = true;
				for (int i = 0; i < args.length - 2; i++) {
					allDone &= !threads[i].isAlive();
				}
			} while (!allDone);
			algorithm = new Algorithm(graph);
			for (int i = 0; i < args.length - 2; i++) {
				algorithm.addModel(threads[i].getModel());
			}
			totalTime1 = algorithm.computeTotalTime(counts);
			algorithm.findConflicts();
			totalTime2 = algorithm.computeTotalTime(counts);
			eTime = System.currentTimeMillis();
			System.out.println(String.format(
					"Time1: %.4f;Time2: %.4f;Diff: %.4f;ExecTime: %.4f",
					totalTime1, totalTime2, (totalTime2 - totalTime1),
					(eTime - sTime) / 1000.0));
		} catch (ArrayIndexOutOfBoundsException | IOException e) {
			e.printStackTrace();
		}
	}
}
