# README #

The repository contains the following elements:

1. README.md - this file,
2. graph.txt - the description of the input graph, used by iccmse.jar,
3. graph-hr.txt - a user-friendly description of the graph stored in graph.txt. 
	For 'Flat corridors', the triple denotes vertices numbers (starting from 0)
	and the common weight for moving in both directions.
	For 'Steep corridors', the quadruple denotes vertices numbers, and the weights
	for moving in the upward and downward directions.
4. iccmse.jar - an executable version of the program,
5. iccmse.bat - a Windows script for running the program,
6. src - a directory containing all the sources.

To run the program it is enough to edit the iccmse.bat file to pass the desired arguments:
e.g. 
java -jar iccmse.jar -cg <output_file>
	where <output_file> denotes the file in which the generated graph will be stored.
java -jar iccmse.jar -sg <input_file> <speeds_combination>
	where <input_file> denotes the file in which the graph is stored,
		  <speeds_combination> is a set of space separated numbers in the range 0-9
		  denoting the speeds which are to be used in the experiment.