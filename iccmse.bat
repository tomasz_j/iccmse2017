@echo off

REM Generates new graph and stores it in file <output_file>.
REM The generation model assumes: 25 vertices, weights belonging to the interval [1, 300],
REM probability of edge existence equal to 0.3, probability of edge steepness equal to 0.3.
REM Usage: java -jar iccmse.jar -cg <output_file> , e.g.:
REM java -jar iccmse.jar -cg graph1.txt 

REM Finds the shortest evacuation paths for given model stored in <input_file>.
REM It is assumed that in each vertex there is 1 miner with given speed. It is also
REM assumed that there are at most 10 speeds, indexed by numbers 0-9 inclusive.
REM Usage: java -jar iccmse.jar -sg <input_file> <list_of_speed_indices> , e.g.:
REM java -jar iccmse.jar -sg graph1.txt 0 2 4 8 9

@echo on